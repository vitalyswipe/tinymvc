﻿<?php

class Controller_Main {
	
	public $load;
	public $model;
	
	function __construct()
	{
		$this->load = new Load();

		// после того, как создана модель
		// создаем метод index, определяющий
		// начальную страницу нашего web-ресурса
		//$this->action_index();
	}
	
	function action_index()
	{	
		$this->load->view('main_view.php', 'template_view.php');
	}
}

?>