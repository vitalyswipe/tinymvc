﻿<?php

class Controller_Admin {
	
	public $load;
	public $model;
	
	function __construct()
	{
		$this->load = new Load();
	}
	
	function action_index()
	{
		$data = array();
		session_start();
		
		if ( $_SESSION['admin'] == "12345" )
		{
			$this->load->view('admin_view.php', 'template_view.php', $data);
		}
		else
		{
			session_destroy();
			header('Location:http://tinymvc.ru/404/');
		}

	}
	
	function action_logout()
	{
		session_start();
		session_destroy();
		header('Location:http://tinymvc.ru/');
	}
}

?>