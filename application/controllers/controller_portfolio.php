﻿<?php

class Controller_Portfolio {
	
	public $load;
	public $model;
	
	function __construct()
	{
		$this->load = new Load();
		$this->model = new Model_Portfolio();
	}
	
	function action_index()
	{
		$data = $this->model->get_data();		
		$this->load->view('portfolio_view.php', 'template_view.php', $data);
	}
}

?>

