﻿<?php

class Routing
{
	private $default_controller = 'Main';
	private $default_action = 'index';
	
	private $controller_prefix = 'Controller_';
	private $action_prefix = 'action_';
	private $model_prefix = 'Model_';
	
	function __construct ()
	{
		//echo "route: ".$_SERVER['REQUEST_URI']."<br>";
		$this->routs = explode('/', $_SERVER['REQUEST_URI']);		

		if (count($this->routs)>3) die('Непредусмотренный маршрут!');
		
		// получаем имя контроллера
		if ( !empty($this->routs[1]) )
		{	
			$this->controller_name = $this->routs[1];
		}
		else
		{
			$this->controller_name = $this->default_controller;
		}
		
		// получаем имя экшена
		if ( !empty($this->routs[2]) )
		{
			$this->action_name = $this->routs[2];	// vj;tv
		}
		else
		{
			$this->action_name = $this->default_action;
		}
		
		// добавляем префиксы
		$this->model_name = $this->model_prefix . $this->controller_name;
		$this->controller_name = $this->controller_prefix . $this->controller_name;
		$this->action_name = $this->action_prefix.$this->action_name;

		$this->run();
	}

	function run()
	{
		/*
		echo "Контроллер: $this->controller_name <br>";
		echo "Экшн: $this->action_name <br>";
		exit;
		*/
		
		// подцепляем файл с классом модели
		$model_file = strtolower($this->model_name).'.php';
		$model_path = "application/models/".$model_file;
		if(file_exists($model_path)) include "models/".$model_file;
		// файла модели может и не быть
		
		// подцепляем файл с классом контроллера
		$controller_file = strtolower($this->controller_name).'.php';
		$controller_path = "application/controllers/".$controller_file;
		if(file_exists($controller_path)) include "controllers/".$controller_file;
		//else die('Такого контроллера не существует!'); // здесь можно кинуть исключение
		else header('Location:http://tinymvc.ru/404/');
		
		// создаем контроллер и вызываем экшн
		$controller = new $this->controller_name;
		$action = $this->action_name;
		
		if(method_exists($controller, $action))
			$controller->$action();
		//else die('Такого экшена не существует!'); // здесь можно кинуть исключение
		else header('Location:http://tinymvc.ru/404/');
	}

}

?>